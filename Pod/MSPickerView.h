//
//  MSPickerView.h
//  MSPickerView
//
//  Created by miss on 2017/10/23.
//  Copyright © 2017年 miss. All rights reserved.
//

// PickerView

#import <UIKit/UIKit.h>

@interface MSPickerView : UIView

// 按钮颜色，默认（50，50，50）
@property (nonatomic, strong) UIColor *buttonColor;

// 标题颜色，默认（50，50，50）
@property (nonatomic, strong) UIColor *titleColor;

// 确定按钮回调
@property (nonatomic, copy) void(^confirmButtonBlock)(NSInteger index);

// 显示 pickerView
- (void)showPickerViewWithArray:(NSArray<NSString *> *)array title:(NSString *)title;

@end

