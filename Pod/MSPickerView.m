//
//  MSPickerView.m
//  MSPickerView
//
//  Created by miss on 2017/10/23.
//  Copyright © 2017年 miss. All rights reserved.
//

#import "MSPickerView.h"

#define MSPK_SCREEN_WIDTH [UIScreen mainScreen].bounds.size.width
#define MSPK_SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height
#define MSPK_BGVIEW_WIDTH self.bgView.frame.size.width
#define MSPK_BGVIEW_HEIGHT self.bgView.frame.size.height
#define MSPK_IPHONEX MSPK_SCREEN_HEIGHT == 812

@interface MSPickerView () <UIPickerViewDelegate, UIPickerViewDataSource>

@property (nonatomic, strong) UIView *bgView;
@property (nonatomic, strong) UIView *topView;
@property (nonatomic, strong) NSArray *array;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) NSArray<UIButton *> *buttonArray;
@property (nonatomic, assign) NSInteger selectIndex;
@property (nonatomic, strong) UIPickerView *pickerView;

@end

@implementation MSPickerView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.frame = CGRectMake(0, 0, MSPK_SCREEN_WIDTH, MSPK_SCREEN_HEIGHT);
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
        
        CGFloat height = MSPK_IPHONEX?(frame.size.height+34):frame.size.height;
        self.bgView = [[UIView alloc]initWithFrame:CGRectMake(0, MSPK_SCREEN_HEIGHT, frame.size.width, height)];
        self.bgView.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.bgView];
        
        [self setUpTopView];
        [self setUpPickerView];
        
    }
    return self;
}

- (void)showPickerViewWithArray:(NSArray *)array title:(NSString *)title {
    [[UIApplication sharedApplication].delegate.window addSubview:self];
    self.array = array;
    [self.pickerView reloadAllComponents];
    self.titleLabel.text = title;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        CGRect rect = self.bgView.frame;
        rect.origin.y = MSPK_SCREEN_HEIGHT-MSPK_BGVIEW_HEIGHT;
        self.bgView.frame = rect;
    }];
}

- (void)setButtonColor:(UIColor *)buttonColor {
    [self.buttonArray enumerateObjectsUsingBlock:^(UIButton * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj setTitleColor:buttonColor forState:UIControlStateNormal];
    }];
}

- (void)setTitleColor:(UIColor *)titleColor {
    self.titleLabel.textColor = titleColor;
}

#pragma mark - 顶部横条view
- (void)setUpTopView {
    self.topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, MSPK_BGVIEW_WIDTH, 45)];
    self.topView.backgroundColor = [UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:1];
    [self.bgView addSubview:self.topView];
    
    UIButton *cancelButton = [self buttonWithFrame:CGRectMake(0, 0, 60, self.topView.frame.size.height) title:@"取消"];
    [cancelButton addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
    [self.topView addSubview:cancelButton];
    UIButton *confirmButton = [self buttonWithFrame:CGRectMake(MSPK_BGVIEW_WIDTH-60, 0, 60, self.topView.frame.size.height) title:@"确定"];
    [confirmButton addTarget:self action:@selector(confirm) forControlEvents:UIControlEventTouchUpInside];
    [self.topView addSubview:confirmButton];
    self.buttonArray = @[cancelButton, confirmButton];
    
    self.titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(60, 0, MSPK_BGVIEW_WIDTH-120, self.topView.frame.size.height)];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.textColor = [UIColor colorWithRed:50/255.0 green:50/255.0 blue:50/255.0 alpha:1];
    [self.topView addSubview:self.titleLabel];
}

- (UIButton *)buttonWithFrame:(CGRect)frame title:(NSString *)title {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    button.frame = frame;
    [button setTitleColor:[UIColor colorWithRed:50/255.0 green:50/255.0 blue:50/255.0 alpha:1] forState:UIControlStateNormal];
    [button setTitle:title forState:UIControlStateNormal];
    return button;
}

- (void)cancel {
    [self removeMSPickerView];
}

- (void)confirm {
    [self removeMSPickerView];
    if (self.confirmButtonBlock) {
        self.confirmButtonBlock(self.selectIndex);
    }
}

- (void)removeMSPickerView {
    [UIView animateWithDuration:0.3 animations:^{
        self.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
        CGRect rect = self.bgView.frame;
        rect.origin.y = MSPK_SCREEN_HEIGHT;
        self.bgView.frame = rect;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma mark - pickerView
- (void)setUpPickerView {
    self.pickerView = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 40, MSPK_BGVIEW_WIDTH, MSPK_BGVIEW_HEIGHT-(MSPK_IPHONEX?80:40))];
    self.pickerView.dataSource = self;
    self.pickerView.delegate = self;
    [self.bgView addSubview:self.pickerView];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.array.count;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 40;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(0, 0, MSPK_BGVIEW_WIDTH, 32);
    label.font = [UIFont systemFontOfSize:22];
    label.text = self.array[row];
    label.textAlignment = NSTextAlignmentCenter;
    ((UILabel *)[self.pickerView.subviews objectAtIndex:1]).backgroundColor = [UIColor grayColor];
    ((UILabel *)[self.pickerView.subviews objectAtIndex:2]).backgroundColor = [UIColor grayColor];    
    return label;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return self.array[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    self.selectIndex = row;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    CGPoint point = [touches.anyObject locationInView:self];
    if (point.y < (MSPK_SCREEN_HEIGHT - MSPK_BGVIEW_HEIGHT)) {
        [self removeMSPickerView];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
